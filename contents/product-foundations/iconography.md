---
name: Iconography
---

One of our values is to create a distinguished GitLab personality that is strong and consistent. Iconography is a powerful visual cue to the user and should reflect our particular sense of style.

## Icon viewer

You can view all of the current icons in GitLab at the following link: [GitLab SVGs](http://gitlab-org.gitlab.io/gitlab-svgs/)

## Usage

Icons are used to stress visual weight for elements with a high priority or to explain the universal knowledge in a simple way.

The level of visual weight from heavy to light is: **Icon + label > Icon > label.**

Todo: Add live component block with code example

## Dos and Dont's

Don't use one icon to refer to different meanings. While consistency is important, we need to remain flexible. Some icons are so basic and ubiquitous that it's difficult to detach them and use them without “conflicts”. An example of a “conflicting” use of icons is the X cross icon used for both failed pipelines and closing modals.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Don't use different icons to refer to one specific meaning.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Don't frequently use the combination of an icon and a label. Use the combination only for the elements with high priority. For instance, we use an icon + a label for the contextual navigation.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Don't use an icon to explain a meaning which is not straight-forward. If an icon is not accompanied by a label, provide a quick explanation for users in a tooltip.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Use whole pixels to create icons in order to align properly to a pixel grid. For additional details on grids, visit our [baseline grid guide](/layout/grid).

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Use rounded strokes.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Simplify icons for clarity and legibility.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |

Make the icons front-face instead of different dimensions.

| **Do** | **Don't** |
| --- | --- |
| Todo: Add example | Todo: Add example |
